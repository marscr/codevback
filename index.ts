//Enviroment Variables
import dotenv from 'dotenv';
import server from './src/server';
import { LogError, LogSuccess } from './src/utils/logger';

//Configuration of the .env file
dotenv.config();

const port = process.env.PORT || 8000;

// *Execute Server
server.listen(port, ()=>{
    LogSuccess(`[Server ON]: Runing in http:localhost:${port}/api`);
});
// *Control SERVER ERROR
server.on('error',(error)=>{
    LogError(`[Server ERROR]: ${error}`);
});
