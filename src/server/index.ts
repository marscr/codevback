//
import express, { Express, Request, Response } from "express";

//Security 
import cors from 'cors';
import helmet from 'helmet';

// TODO HTTPS

// Root Router
import router from '../routes'


// Create Express Server
const server: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define SERVER url to "/api" and execute rootRouter from 'index.ts' in routes
//From this poing onover: http://localhost:posrt/api/...
server.use(
    '/api',
    router
    );

// TODO: Mongoose Connection

// Security Config
server.use(helmet());
server.use(cors());

// Content Type Config
server.use(express.urlencoded({ extended: true, limit: '50mb'}));
server.use(express.json({ limit: '50mb' }));

//* Redirection Config
// http://localhost:port --> http://localhost:port/api/
server.get('/',(req: Request, res: Response)=>{
    res.redirect('/api'); 
});

export default server;