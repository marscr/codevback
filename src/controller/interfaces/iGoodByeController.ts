import { BasicResponse } from "../types";

export interface IGoddByeController{
    getMessage(name?:string): Promise<BasicResponse>
}