/**
 * Basic Json Response for Controllers
 */
export type BasicResponse = {
    message: string;
}
/**
 * Error Response for Controllers
 */
export type ErrorResponse = {

}