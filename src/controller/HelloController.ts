import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces/iHelloController";
import { LogSuccess } from "../utils/logger";

export class HelloController implements IHelloController{
    // Method from the interface
    public async getMessage(name?: string | undefined): Promise<BasicResponse> {
        LogSuccess("[api/hello] Get Request ");

        return{
            message: `Hello ${name || "World"}!`
        }
    }
    
}