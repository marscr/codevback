import { BasicResponse } from "./types";
import { IGoddByeController } from "./interfaces/iGoodByeController";
import { LogSuccess } from "../utils/logger";

export class GoodByeController implements IGoddByeController{
    public async getMessage(name?: string | undefined, date?: Date | undefined): Promise<BasicResponse> {
        date= new Date();
        LogSuccess(`[api/goodbye] Get Request...Name: ${name} ... Date: ${date}`);
        return{
            message: `God Bye ${name || "World"}! Date: ${date || new Date()}`
        }
    }
}