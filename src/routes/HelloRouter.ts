import express, { Request, Response } from "express";
import { HelloController } from "../controller/HelloController";
import { request } from "http";
import { LogInfo } from "../utils/logger";
import { BasicResponse } from "../controller/types";

//Router from express 
let HelloRouter = express.Router();

//http://localhost:port/api/hello?name=Nombre/
HelloRouter.route('/')
    // GET:
    .get(async(req: Request, res: Response)=>{
        //otain a Query Param
        let name: any = req?.query?.name;
        LogInfo(`Query Param: ${name}`)
        //res.send('Hello  '+ name +'<a href="../">Back</a>');
        // Controller Instance to execute method
        const controller: HelloController = new HelloController();
        //Obtain Response
        const response = await controller.getMessage(name);
        //sent te response to the client
       return res.send(response); 
    });

    //Export Hello Router
export default HelloRouter;