import express,{ Request, Response } from "express";
import { GoodByeController } from "../controller/GoodByeController";
import { request } from "http";
import { LogInfo } from "../utils/logger";
import { BasicResponse } from "../controller/types";

//Router from express 
let GodByeRouter = express.Router();
//http://localhost:port/api/goodbye?name=Nombre?date=Fecha/
GodByeRouter.route('/')
    // GET:
    .get(async(req: Request, res: Response)=>{
        //otain a Query Param
        let name: any = req?.query?.name;
        let date: any = new Date();
        LogInfo(`Query Param Name: ${name} , Date: ${date}`)
        //res.send('Hello  '+ name +'<a href="../">Back</a>');
        // Controller Instance to execute method
        const controller: GoodByeController = new GoodByeController();
        //Obtain Response
        const response = await controller.getMessage(name);
        //sent te response to the client
       return res.send(response); 
    });

    //Export Hello Router
export default GodByeRouter;