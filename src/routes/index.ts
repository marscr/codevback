/**
 * Root Router
 * Router Redirections
 */

import express,{ Request, Response } from "express";
import HelloRouter from "./HelloRouter";
import { LogInfo } from "../utils/logger";
import GodByeRouter from "./GoodByeRouter";

//Server Instance
let server = express();

//Router Instance
let rootRouter = express.Router();

//Activates when Request to http://localhost:port/api
rootRouter.get('/',(req: Request,res: Response)=>{
    LogInfo('GET: http://localhost:8500/api/')
    res.send('API Restful using:  Express + TS + Nodemon + Jest + Swagger + Mongoose ')
});

//Rredirections to Routers & Controllers
server.use('/',rootRouter); //http://localhost:port/api/
server.use('/hello',HelloRouter); //http://localhost:port/api/hello --> HelloRouter
server.use('/goodbye',GodByeRouter); //http://localhost:port/api/goodbye --> GoodByeRouter
// Create more routes

export default server;

