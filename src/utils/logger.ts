//Message notifying that something is happening
export const LogInfo = (message: string ) => {
    console.log(`Info: ${message}`); 
}
//Message when something finishes succesfully
export const LogSuccess = (message: string ) => {
    console.log(`Success: ${message}`); 
}
//Message notifying that something may fail
export const LogWarning = (message: string ) => {
    console.log(`Warning: ${message}`); 
}
//Message when something finishes with errors
export const LogError = (message: string ) => {
    console.log(`Error: ${message}`); 
}