# Node Code Vierifier Backend
Developed by Marlon Araya Seas
Following [Open Bootcamp MERN Course](https://campus.open-bootcamp.com/cursos/62)

## Dependencies
- Dotenv
    Manage Enviroment Variable
- Express
    NodeJS Framework
### Dev Dependencies
- Webpack
    To prepare the code 
- Jest
    JavaScript Test Framework
- Eslint
    For Best Practices 
- TypeScript
    Javascript SuperSet
## Scripts
- build
    Transpile TS to JS in Dist Folder 
- start
    Run the Dist Files 
- dev
    Run a server with nodemon to watch changes and refresh the build  
- serve:coverage
    Run the Test and execute a Server when we can see the coverage of the code

## Enviroment Variables
- ### PORT
    This variable is used to assign the PORT